﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
   
    public class Start
    {
        public int year { get; set; }
        public int month { get; set; }
    }

    public class End
    {
        public int year { get; set; }
        public int month { get; set; }
    }

    public class Active
    {
        public Start start { get; set; }
        public End end { get; set; }
    }

    public class ActivityInfo
    {
        public List<Active> active { get; set; }
        public List<object> missing { get; set; }
    }

    public class DateMinMax
    {
        public string max_txn_date { get; set; }
        public string min_txn_date { get; set; }
    }

    public class Period
    {
        public int pk { get; set; }
        public string begin_date { get; set; }
        public string end_date { get; set; }
        public string begin_balance { get; set; }
        public string end_balance { get; set; }
           }
    public class BankAccount
    {
        public string name { get; set; }
        public ActivityInfo activity_info { get; set; }
        public DateMinMax date_min_max { get; set; }
        public int book_pk { get; set; }
        public int pk { get; set; }
        public string account_type { get; set; }
        public string account_holder { get; set; }
        public string account_number { get; set; }
        public string holder_zip { get; set; }
        public string holder_state { get; set; }
        public string holder_city { get; set; }
        public string holder_address_1 { get; set; }
        public string holder_address_2 { get; set; }
        public List<Period> periods { get; set; }
        
    }
    public class AnalyticsBankData
    {

    }
}
