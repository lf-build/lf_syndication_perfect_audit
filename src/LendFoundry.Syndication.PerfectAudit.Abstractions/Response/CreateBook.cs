﻿namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class CreateBookDetail
    {
        public string name { get; set; }
        public string created { get; set; }
        public int pk { get; set; }
        public bool is_public { get; set; }
        public string owner_email { get; set; }
    }

    public class CreateBook :CommonResponse
    {
       public CreateBookDetail cbresponse { get; set; }
    }
}
