﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class BookInformationDetail
    {
        public string name { get; set; }
        public string created { get; set; }
        public Dictionary<string,BankAccount> bank_accounts { get; set; }
        public List<Doc> docs { get; set; }
        public int pk { get; set; }
        public bool is_public { get; set; }
        public string owner_email { get; set; }
    }
    public class BookInformation :CommonResponse
    {
        public BookInformationDetail bookinformationdetailresponse { get; set; }
    }
}
