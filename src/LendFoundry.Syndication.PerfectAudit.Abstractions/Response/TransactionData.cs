﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class TransactionData
    {
        public Dictionary<string,BankAccount> bank_accounts { get; set; }
        public List<Txn> txns { get; set; }
        public Dictionary<string, UploadDoc> uploaded_docs { get; set; }
    }
    public class Transaction :CommonResponse
    {
      public TransactionData tbresponse { get; set; }
    }
    public class UploadDoc
    {
        public string name { get; set; }
        public int pages { get; set; }
    }
    public class Txn
    {
        public int page_idx { get; set; }
        public bool is_recurring { get; set; }
        public int bank_account_pk { get; set; }
        public string amount { get; set; }
        public List<int> bbox { get; set; }
        public string txn_date { get; set; }
        public int page_doc_pk { get; set; }
        public int pk { get; set; }
        public int uploaded_doc_pk { get; set; }
        public string description { get; set; }
    }
}
