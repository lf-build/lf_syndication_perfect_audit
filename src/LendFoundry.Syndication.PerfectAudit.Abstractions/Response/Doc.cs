﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class Doc
    {
        public int pages { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public int pk { get; set; }
    }
}
