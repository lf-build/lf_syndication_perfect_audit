﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    /// <summary>
    /// AnalyticsData
    /// </summary>
    public class AnalyticsData
    {
        public Dictionary<string,string> average_by_month { get; set; }
        public string average_daily_balance { get; set; }
        public string average_daily_balance_weekday { get; set; }
        public int pk { get; set; }
        public string name { get; set; }
        public List<BankAccountData> bank_accounts { get; set; }
        public Dictionary<string, string> average_deposit_by_month { get; set; }
        public Dictionary<string, string> average_daily_balance_by_month { get; set; }
        public Dictionary<string, string> average_daily_balance_weekday_by_month { get; set; }
        public Dictionary<string,string> estimated_revenue_by_month { get; set; }

    }
    public class Analytics:CommonResponse
    {
        public AnalyticsData adresponse { get; set; }
    }
    public class PeriodData :Period
    {
        public string primary_recon_error_reason { get; set; }
        public string secondary_recon_error_reason { get; set; }
        public Dictionary<string,int> period_month_days { get; set; }
        public Dictionary<string,int> period_month_txns { get; set; }
    }
    public class BankAccountData
    {
        public Dictionary<string, string> average_by_month { get; set; }
        public string average_daily_balance_weekday { get; set; }
        public Dictionary<string, string> average_deposit_by_month { get; set; }
        public Dictionary<string, string> daily_balances { get; set; }
        public object nsf_transactions { get; set; }
        public int nsf_count { get; set; }
        public Dictionary<string, List<Txn>> deposit_max_by_month { get; set; }
        public Dictionary<string, List<Txn>> deposit_min_by_month { get; set; }
        public Dictionary<string, List<string>> negative_balances_by_month { get; set; }
        public List<Txn> outside_source_deposits { get; set; }
        public List<Period> period_balance_mismatches {get;set;}
        public object interbank_transactions {get;set;}
        public string average_daily_balance { get; set; }
        public string name { get; set; }
        public ActivityInfo activity_info { get; set; }
        public DateMinMax date_min_max { get; set; }
        public int book_pk { get; set; }
        public int pk { get; set; }
        public Dictionary<string,string> average_daily_balance_by_month { get; set; }
        public Dictionary<string,string> average_daily_balance_weekday_by_month { get; set; }
        public Dictionary<string,string> estimated_revenue_by_month { get; set; }
        public string account_type { get; set; }
        public string account_holder { get; set; }
        public string account_number { get; set; }
        public string holder_zip { get; set; }
        public string holder_state { get; set; }
        public string holder_city { get; set; }
        public string holder_address_1 { get; set; }
        public string holder_address_2 { get; set; }
        public List<PeriodData> periods { get; set; }
        public Dictionary<string, List<string>> negative_balances_by_month_weekday { get; set; }
        public object round_number_txns { get; set; }
        public object alternative_lender_transactions { get; set; }
        public int total_days { get; set; }
        public Dictionary<string, string> deposits_sum_by_month { get; set; }

    }
}
