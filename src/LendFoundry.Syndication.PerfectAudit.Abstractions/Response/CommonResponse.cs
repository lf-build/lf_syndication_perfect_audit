﻿namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class CommonResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public Meta meta { get; set; }
        public Response response { get; set; }
    }
    public class Meta
    {
        public int status { get; set; }
        public string msg { get; set; }
        public int code { get; set; }
    }
    public class Response
    {
        public int status { get; set; }
        public string message { get; set; }
    }
}
