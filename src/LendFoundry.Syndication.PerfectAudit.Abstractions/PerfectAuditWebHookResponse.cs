﻿namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class PerfectAuditWebHookResponse
    {
        public string status { get; set; }
        public string book_pk { get; set; }

        public string uploaded_doc_pk { get; set; }
    }
}
