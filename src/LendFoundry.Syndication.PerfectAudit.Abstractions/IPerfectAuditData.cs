﻿

using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public interface IPerfectAuditData :IAggregate
    { 
         string EntityId { get; set; }
         string EntityType { get; set; }
         string BookName { get; set; }
         string BookPk { get; set; }
         List<Dictionary<string, string>> Documents { get; set; }
         string VerificationStatus { get; set; }
         DateTime CreatedDate { get; set; }
    }
}
