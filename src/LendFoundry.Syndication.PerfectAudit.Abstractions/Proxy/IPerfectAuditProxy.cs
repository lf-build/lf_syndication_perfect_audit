﻿
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions.Proxy
{
    public interface IPerfectAuditProxy
    {
        Task<CreateBook> CreateBook(string name);
        Task<CommonResponse> UploadStatement(string pk, byte[] statement,string name);
        Task<BookInformation> GetBookInformation(string pk);
        Task<CreateBook> SetBook(string pk, bool isPublic);
        Task<Transaction> GetTransaction(string bookpk);
        Task<Analytics> GetAnalyticalData(string pk);
        Task<CommonResponse> DeleteBook(string bookid);
        Task<CommonResponse> DeleteDocument(string bookid);
        Task<CommonResponse> CallWebHook(WebHookRequest request);
        Task<CommonResponse> UploadStatement(string pk, List<FileuploadData> statement/*, string name*/);
    }
}
