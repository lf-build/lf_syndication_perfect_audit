﻿using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

using System.Text;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions.Proxy
{
    public class PerfectAuditProxy : IPerfectAuditProxy
    {
        public PerfectAuditProxy(PerfectAuditConfiguration configuration, ILogger logger)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            Configuration = configuration;
            Logger = logger;
        }
        private PerfectAuditConfiguration Configuration { get; }
        private ILogger Logger { get; }
        public async Task<CreateBook> CreateBook(string name)
        {
            var url = Configuration.BaseUrl + Configuration.CreateBookUrl;
            url = Configuration.UseSimulation ? Configuration.SimulationUrl : url;
            RestClient restClient = new RestClient(url);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Content-Type", "application/json");
            if (Configuration.UseSimulation)
            {
                //restRequest.AddParameter("name", name);
                restRequest.AddQueryParameter("is_public", Configuration.isPublic.ToString());
            }
            else
            {
                restRequest.AddParameter("application/json", "{\"name\":\"" + name + "\",\"is_public\":\"" + Configuration.isPublic + "\"}", ParameterType.RequestBody);
            }
            return await Task.Run(() => ExecuteRequest<CreateBook>(restClient, restRequest));
        }
        public async Task<CommonResponse> UploadStatement(string pk,byte[] statement,string name)
        {
            var restClient = new RestClient(Configuration.BaseUrl + Configuration.UploadDocumentUrl);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Content-Type", "multipart/form-data");
            restRequest.AddParameter("pk", pk);
            restRequest.AddFile("upload", statement, name, "multipart/form-data");
            
            return await Task.Run(() => ExecuteRequest<CommonResponse>(restClient, restRequest));
        }
        public async Task<BookInformation> GetBookInformation(string pk)
        {
            var restClient = new RestClient(Configuration.BaseUrl + Configuration.GetBookInformationUrl);
            var restRequest = new RestRequest(Method.POST);
           
            restRequest.AddHeader("Content-Type", "application/json");
          
            restRequest.AddParameter("application/json", "{\"pk\":\"" + pk + "\"}", ParameterType.RequestBody);
           
            return await Task.Run(() => ExecuteRequest<BookInformation>(restClient, restRequest));
        }
        public async Task<CreateBook> SetBook(string pk,bool isPublic)
        {
            var restClient = new RestClient(Configuration.BaseUrl + Configuration.SetBookUrl);
            var restRequest = new RestRequest(Method.POST);
           
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("application/json", "{\"pk\":\"" + pk + "\",\"is_public\":\"" + isPublic + "\"}", ParameterType.RequestBody);
            return await Task.Run(() => ExecuteRequest<CreateBook>(restClient, restRequest));
        }
        public async Task<Transaction> GetTransaction(string bookpk)
        {
            var url = Configuration.BaseUrl + Configuration.GetTransactionUrl;
            url = Configuration.UseSimulation ? Configuration.SimulationUrl : url;
            RestClient restClient = new RestClient(url);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Content-Type", "application/json");
            if (Configuration.UseSimulation)
            {
                restRequest.AddQueryParameter("book_pk", bookpk);
            }
            else
            {
                restRequest.AddParameter("application/json", "{\"book_pk\":\"" + bookpk + "\"}", ParameterType.RequestBody);
            }
            return await Task.Run(() => ExecuteRequest<Transaction>(restClient, restRequest));
        }
        public async Task<Analytics> GetAnalyticalData(string pk)
        {
            var url = Configuration.BaseUrl + Configuration.GetAnalyticsDataUrl;
            url = Configuration.UseSimulation ? Configuration.SimulationUrl : url;
            RestClient restClient = new RestClient(url);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Content-Type", "application/json");
            if (Configuration.UseSimulation)
            {
                restRequest.AddParameter("pk", pk);
            }
            else
            {
                restRequest.AddParameter("application/json", "{\"pk\":\"" + pk + "\"}", ParameterType.RequestBody);
            }
            return await Task.Run(() => ExecuteRequest<Analytics>(restClient, restRequest));
        }
        public async Task<CommonResponse> DeleteBook(string bookid)
        {
            var restClient = new RestClient(Configuration.BaseUrl + Configuration.DeleteBookUrl);
            var restRequest = new RestRequest(Method.POST);
            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddHeader("Content-Type", "application/json");
        
          
            restRequest.AddParameter("application/json", "{\"book_id\":\"" + bookid + "\"}", ParameterType.RequestBody);

            return await Task.Run(() => ExecuteRequest<CommonResponse>(restClient, restRequest));
        } 
      
        public async Task<CommonResponse> DeleteDocument(string bookid)
        {
            var restClient = new RestClient(Configuration.BaseUrl + Configuration.DeleteDocumentUrl);
            var restRequest = new RestRequest(Method.POST);
            
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter("application/json", "{\"book_id\":\"" + bookid + "\"}", ParameterType.RequestBody);
            return await Task.Run(() => ExecuteRequest<CommonResponse>(restClient, restRequest));
        }
      
        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var plainTextBytes = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Configuration.ApiKey + ":" + Configuration.ApiSecret));
            var headerAuthKey = "Basic " + plainTextBytes;
            request.AddHeader("Authorization", headerAuthKey);
            Logger.Info("===>" + client.BaseUrl.AbsoluteUri);
            Logger.Info("===>insdie https");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            Logger.Info("===> Executing client.Execute request...");
            Logger.Info("requset", request);
            Logger.Info($"Resource : { request.Resource} ");
            
            Logger.Info("client", client);
            var response = client.Execute(request);
          
            Logger.Info("Response", response);
            Logger.Info("Content", response.Content);
            Logger.Info("ErrorException", response.ErrorException);
            Logger.Info("ErrorMessage", response.ErrorMessage);
            Logger.Info($"ErrorException Request: {response.Request} ");
            Logger.Info($"ErrorException Request: {response.Headers} ");
            Logger.Info($"ErrorException Server: {response.Server} ");
            Logger.Info($"ErrorException RawBytes: {response.RawBytes} ");

            Logger.Info($"Response:ErrorException");
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
            {
                Logger.Info($"ErrorException Data: {response.ErrorException.Data} ");
                Logger.Info($"ErrorException HelpLink: {response.ErrorException.HelpLink} ");
                Logger.Info($"ErrorException StackTrace: {response.ErrorException.StackTrace} ");
                Logger.Info($"ErrorException TargetSite: {response.ErrorException.TargetSite} ");
                Logger.Info($"ErrorException Source: {response.ErrorException.Source} ");
                Logger.Info($"ErrorException InnerException: {response.ErrorException.InnerException} ");

                Logger.Info($"ErrorException TargetSite: {response.ErrorException.InnerException} ");
                Logger.Info($"ErrorException ErrorMessage: {response.ErrorMessage} ");
                Logger.Info($"ErrorException Request: {response.Request} ");
                Logger.Info($"ErrorException Request: {response.Headers} ");
                Logger.Info($"ErrorException Server: {response.Server} ");
                Logger.Info($"ErrorException RawBytes: {response.RawBytes} ");

                Logger.Error($"[PerfectAudit] Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}. Error:", response.ErrorException.Message);
                throw new PerfectAuditException(response.ErrorException.Message);

            }

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                Logger.Error($"[PerfectAudit] not Completed Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}.. Error:", response.Content);
                throw new PerfectAuditException(response.Content);
            }

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
            {
                Logger.Error($"[PerfectAudit] methodnotallowed Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}.. Error:", response.StatusDescription);
                throw new PerfectAuditException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error($"[PerfectAudit] not OK Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}. Error:", response.Content ?? "");
                throw new PerfectAuditException(response.Content);
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (PerfectAuditException exception)
            {
                Logger.Error($"[PerfectAudit] catch block executed with Error occured when  { request.Resource } was called with headerAuthKey {headerAuthKey}. Error:", response.Content);
                throw new PerfectAuditException("Unable to deserialize:" + response.Content, exception);
            }
        }

        public async Task<CommonResponse> CallWebHook(WebHookRequest request)
        {
            var url = Configuration.WebHookUrl;

            RestClient restClient = new RestClient(url);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddJsonBody(request);
            //restRequest.AddParameter("application/json", "{\"status\":\"" + request.Status + "\",\"book_pk\":" + request.book_pk+ ",\"uploaded_doc_pk\":" + request.uploaded_doc_pk + "}", ParameterType.RequestBody);

            return await Task.Run(() => ExecuteRequest<CommonResponse>(restClient, restRequest));
        }
        public async Task<CommonResponse> UploadStatement(string pk, List<FileuploadData> statement/*, string name*/)
        {
            Logger.Info("Executing UploadStatement in perfect audit proxy service");
            var restClient = new RestClient(Configuration.BaseUrl + Configuration.UploadDocumentUrl);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Content-Type", "multipart/form-data");
            restRequest.AddParameter("pk", pk);
            int index = 0;
            foreach (var item in statement)
            {
                restRequest.AddFile("upload[" + index + "]", item.Filedetails, item.FileName, "multipart/form-data");
                index++;
            }

            return await Task.Run(() => ExecuteRequest<CommonResponse>(restClient, restRequest));
        }
    }
}

