﻿

using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class PerfectAuditData : Aggregate, IPerfectAuditData
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }

        public string BookName { get; set; }
        public string BookPk { get; set; }
        public List<Dictionary<string,string>> Documents { get; set; }
        public string VerificationStatus {get;set;}

        public DateTime CreatedDate { get; set; }
    }
}
