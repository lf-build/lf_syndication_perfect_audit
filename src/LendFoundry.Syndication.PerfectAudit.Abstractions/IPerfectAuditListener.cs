﻿namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public interface IPerfectAuditListener
    {
        void Start();
    }
}
