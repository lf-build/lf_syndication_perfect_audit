﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    [Serializable] 
    public class PerfectAuditException : Exception
    {
        public string ErrorCode { get; set; }

        public PerfectAuditException()
        {
        }
        public PerfectAuditException(string message) : this(null, message, null)
        {
        }

        public PerfectAuditException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        public PerfectAuditException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }
        public PerfectAuditException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        protected PerfectAuditException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
