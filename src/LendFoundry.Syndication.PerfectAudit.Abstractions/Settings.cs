﻿using System;


namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "perfectaudit";
    }
}
