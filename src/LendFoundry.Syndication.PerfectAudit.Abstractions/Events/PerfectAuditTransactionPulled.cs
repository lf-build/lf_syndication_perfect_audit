﻿using LendFoundry.SyndicationStore.Events;
namespace LendFoundry.Syndication.PerfectAudit.Abstractions.Events
{
    public class PerfectAuditTransactionPulled : SyndicationCalledEvent
    {
    }
}
