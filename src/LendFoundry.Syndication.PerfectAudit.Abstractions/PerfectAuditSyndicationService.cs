﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.PerfectAudit.Abstractions.Proxy;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class PerfectAuditSyndicationService : IPerfectAuditSyndicationService
    {

        public PerfectAuditSyndicationService(ILogger logger, IPerfectAuditProxy perfectAuditProxy, ILookupService lookupService)
        {
            Logger = logger;
            PerfectAuditProxy = perfectAuditProxy;
            LookupService = lookupService;

        }
        private ILookupService LookupService { get; }
        private IEventHubClient EventHub { get; }
        private ILogger Logger { get; }
        private IPerfectAuditProxy PerfectAuditProxy { get; }

        public async Task<CreateBook> CreateBook(string entityType, string entityId)
        {
            return await PerfectAuditProxy.CreateBook(entityId);
        }

        public async Task<CommonResponse> DeleteBook(string entityType, string entityId, string bookpk)
        {
            return await PerfectAuditProxy.DeleteBook(bookpk);
        }

        public async Task<Transaction> GetTransaction(string entityType, string entityId, string bookpk)
        {
            return await PerfectAuditProxy.GetTransaction(bookpk);
        }
        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, string pk, byte[] statement,string name)
        {
            return await PerfectAuditProxy.UploadStatement(pk, statement,name);
        }
        public async Task<Analytics> GetAnalyticalData(string entityType, string entityId, string bookpk)
        {
            return await PerfectAuditProxy.GetAnalyticalData(bookpk);
        }

        public async Task<CommonResponse> CallWebHook(WebHookRequest request)
        {
            return await PerfectAuditProxy.CallWebHook(request);
        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, string pk, List<FileuploadData> statement/*, string name*/)
        {
            return await PerfectAuditProxy.UploadStatement(pk, statement/*, name*/);
        }
    }
}
