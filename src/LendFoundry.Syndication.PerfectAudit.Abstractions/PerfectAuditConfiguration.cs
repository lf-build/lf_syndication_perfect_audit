﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class PerfectAuditConfiguration : IDependencyConfiguration
    {
        public string BaseUrl { get; set; } = "https://www.perfectaudit.com/api/v1";
        public string CreateBookUrl { get; set; } = "/book/add";
        public bool isPublic { get; set; }
        public string ApiKey { get; set; } = "jignesh.j@sigmainfo.net";
        public string ApiSecret { get; set; } = "Jaihind12#$";
        public string UploadDocumentUrl { get; set; } = "/book/upload";
        public string GetBookInformationUrl { get; set; } = "book/info";
        public string UpdateBookUrl { get; set; } = "/book/update";
        public string GetTransactionUrl { get; set; } = "/transaction";
        public string GetAnalyticsDataUrl { get; set; } = "/book/summary";
        public string DeleteBookUrl { get; set; } = "/book/remove";
        public string DeleteDocumentUrl { get; set; } = "/document/remove";
        public string SetBookUrl { get; set; } = "/book/update";
        public List<EventConfiguration> events { get; set; }
        public string SimulationUrl { get; set; }
        public bool UseSimulation { get; set; }
        public string WebHookUrl { get; set; } = "";
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }


    }
}
