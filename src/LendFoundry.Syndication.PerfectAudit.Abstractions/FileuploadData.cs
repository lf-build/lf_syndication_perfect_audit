﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class FileuploadData : IFileuploadData
    {
      public byte[] Filedetails { get; set; }
      public string FileName { get; set; }

    }

    public interface IFileuploadData
    {
        byte[] Filedetails { get; set; }
        string FileName { get; set; }
    }
}
