﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class UploadStatementDetails : IUploadStatementDetails
    {
        public List<FileuploadData> Filedetails { get; set; }
       

    }

    public interface IUploadStatementDetails
    {
        List<FileuploadData> Filedetails { get; set; }

    }
}
