﻿
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public interface IPerfectAuditRepository : IRepository<IPerfectAuditData>
    {
        Task<IPerfectAuditData> GetByEntityId(string entityType, string entityId);
        Task<IPerfectAuditData> GetByBookId(string bookPk);
    }
}
