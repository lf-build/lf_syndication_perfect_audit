﻿
using System.Collections.Generic;
using System.Threading.Tasks;


namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public interface IPerfectAuditSyndicationService
    {
        Task<CreateBook> CreateBook(string entityType, string entityId);
        Task<CommonResponse> DeleteBook(string entityType, string entityId, string bookpk);
        Task<Transaction> GetTransaction(string entityType, string entityId, string bookpk);
        Task<Analytics> GetAnalyticalData(string entityType, string entityId, string bookpk);
        Task<CommonResponse> UploadStatement(string entityType, string entityId, string pk, byte[] statement,string name);
        Task<CommonResponse> CallWebHook(WebHookRequest request);
        Task<CommonResponse> UploadStatement(string entityType, string entityId, string pk, List<FileuploadData> statement/*, string name*/);

    }
}