﻿namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class EventConfiguration
    {
        public string EntityId { get; set; }
        public string Response { get; set; }
        public string EntityType { get; set; }
        public string Name { get; set; }
        public string CompletionEventName { get; set; }
        public string DocumentNameFormat { get; set; }
    }
}
