﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    public class WebHookRequest
    {
        public string Status = "VERIFICATION_COMPLETE";
        public int book_pk = 11917;
        public int uploaded_doc_pk = 57733;
    }
}
