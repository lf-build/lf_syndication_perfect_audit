﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.PerfectAudit.Client
{
    public interface IPerfectAuditServiceClientFactory
    {
        #region Public Methods

        IPerfectAuditService Create(ITokenReader reader);

        #endregion Public Methods
    }
}