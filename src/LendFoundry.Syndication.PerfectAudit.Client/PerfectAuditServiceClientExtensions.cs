﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;
namespace LendFoundry.Syndication.PerfectAudit.Client
{
    /// <summary>
    /// all Service injection
    /// </summary>
    public static class PerfectAuditServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="endpoint">endpoint of service</param>
        /// <param name="port">port of service</param>
        /// <returns>Service</returns>
        public static IServiceCollection AddPerfectAuditServic(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPerfectAuditServiceClientFactory>(p => new PerfectAuditServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPerfectAuditServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPerfectAuditServic(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IPerfectAuditServiceClientFactory>(p => new PerfectAuditServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IPerfectAuditServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPerfectAuditServic(this IServiceCollection services)
        {
            services.AddSingleton<IPerfectAuditServiceClientFactory>(p => new PerfectAuditServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IPerfectAuditServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        #endregion Public Methods
    }
}
