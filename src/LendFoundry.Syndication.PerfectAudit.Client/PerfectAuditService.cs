﻿using System;
using System.Threading.Tasks;
using LendFoundry.Syndication.PerfectAudit.Abstractions;

using RestSharp;
using LendFoundry.Foundation.Services; 
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.PerfectAudit.Client
{
    public class PerfectAuditService : IPerfectAuditService
    {
        #region Public Constructors

        public PerfectAuditService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Public Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        public Task Add(string entityType, string entityId, byte[] statement, string name, bool reCreateBook)
        {
            throw new NotImplementedException();
        }



        #endregion Private Properties

        public async Task<CreateBook> CreateBook(string entityType, string entityId)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/createbook", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
           return await Client.ExecuteAsync<CreateBook>(request);
        }

        public async Task<Transaction> GetTransactionData(string entityType, string entityId)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/transactions", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<Transaction>(request);
        }
        public async Task<Analytics> GetSummaryDataByBookId(string bookId)
        {
            var request = new RestRequest("/{bookid}/summary", Method.GET);
            request.AddUrlSegment("bookid", bookId);
            return await Client.ExecuteAsync<Analytics>(request);
        }
        public async Task UploadStatement(string entityType, string entityId, byte[] statement, string name, bool reCreateBook)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/uploadstatement/{*reCreateBook}", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddFile("file", statement, name, "application/octet-stream");
            request.AddUrlSegment("name", name);
            request.AddUrlSegment("reCreateBook", reCreateBook.ToString());
            await Client.ExecuteAsync(request);
        }

        public async Task<Transaction> GetTransactionByBookId(string bookId)
        {
            var request = new RestRequest("/{bookid}/transactions", Method.GET);
            request.AddUrlSegment("bookid", bookId);
            return await Client.ExecuteAsync<Transaction>(request);
        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, UploadStatementDetails files, /*string name, */string bookPk)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/uploadstatement/{bookPk}/multiple", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("bookPk", bookPk.ToString());
            request.AddJsonBody(files);
            //request.AddFile("file", statement, name, "application/octet-stream");
                        
            return  await Client.ExecuteAsync<CommonResponse>(request);
        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/uploadstatement/{bookPk}/single", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("bookPk", bookPk);
            request.AddFile("file", statement, name, "application/octet-stream");
            return await Client.ExecuteAsync<CommonResponse>(request);
        }
    }
}
