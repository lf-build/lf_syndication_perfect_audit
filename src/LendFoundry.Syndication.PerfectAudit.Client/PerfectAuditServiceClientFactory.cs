﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.PerfectAudit.Client
{
    public class PerfectAuditServiceClientFactory : IPerfectAuditServiceClientFactory
    {
        #region Public Constructors

        public PerfectAuditServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public PerfectAuditServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        #endregion Public Constructors

        #region Private Properties

        private string Endpoint { get; }
        private int Port { get; }
        private IServiceProvider Provider { get; }

        private Uri Uri { get; }

        #endregion Private Properties

        #region Public Methods
        
        public IPerfectAuditService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("perfectaudit");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new PerfectAuditService(client);
        }
        #endregion Public Methods
    }
}
