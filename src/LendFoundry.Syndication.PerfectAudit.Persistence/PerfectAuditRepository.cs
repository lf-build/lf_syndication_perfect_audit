﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using System;

namespace LendFoundry.Syndication.PerfectAudit.Persistence
{
    public class PerfectAuditRepository : MongoRepository<IPerfectAuditData, PerfectAuditData>, IPerfectAuditRepository
    {
        static PerfectAuditRepository()
        {
            BsonClassMap.RegisterClassMap<PerfectAuditData>(map =>
            {
                map.AutoMap();
                var type = typeof(PerfectAuditData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public PerfectAuditRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "perfectAuditData")
        {
            CreateIndexIfNotExists("perfectAuditData",
                Builders<IPerfectAuditData>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }

        public async Task<IPerfectAuditData> GetByBookId(string bookPk)
        {
            return await Query.FirstOrDefaultAsync(a => a.BookPk == bookPk);
        }

        public async Task<IPerfectAuditData> GetByEntityId(string entityType, string entityId)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId);
        }
    }
}
