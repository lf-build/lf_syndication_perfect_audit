﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Net.Http.Headers;
#endif


namespace LendFoundry.Syndication.PerfectAudit.Api.Controllers
{
    /// <summary>
    /// ApiController
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// ApiController constructor
        /// </summary>
        /// <param name="perfectAuditservice"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public ApiController(IPerfectAuditService perfectAuditservice, ILogger logger) : base(logger)
        {
            if (perfectAuditservice == null)
                throw new ArgumentNullException(nameof(perfectAuditservice));
            PerfectAuditService = perfectAuditservice;
        }
        private IPerfectAuditService PerfectAuditService { get; }

        /// <summary>
        /// CreateBook
        /// </summary>
        /// <param name="entitytype"></param>
        /// <param name="entityid"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/createbook")]
#if DOTNET2
        [ProducesResponseType(typeof(CreateBook), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> CreateBook(string entitytype, string entityid)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfectAuditService.CreateBook(entitytype, entityid);
                    return Ok(response);
                }
                catch (PerfectAuditException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// UploadStatement
        /// </summary>
        /// <param name="entitytype"></param>
        /// <param name="entityid"></param>
        /// <param name="file"></param>
        /// <param name="reCreateBook"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/uploadstatement/{*reCreateBook}")]
#if DOTNET2
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> UploadStatement(string entitytype, string entityid, IFormFile file, bool reCreateBook)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (file == null || file.Length <= 0)
                        throw new ArgumentException("Value cannot be null or whitespace.", nameof(file));
                    var filename = ContentDispositionHeaderValue
                        .Parse(file.ContentDisposition)
                        .FileName
                        .Trim('"');

                    await
                        PerfectAuditService.UploadStatement(entitytype, entityid,
                            file.OpenReadStream().ReadAsBytes(), filename, reCreateBook);
                    return  Ok(204);
                }
                catch (PerfectAuditException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetTransactions
        /// </summary>
        /// <param name="entitytype"></param>
        /// <param name="entityid"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/transactions")]
#if DOTNET2
        [ProducesResponseType(typeof(Transaction), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetTransactions(string entitytype, string entityid)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfectAuditService.GetTransactionData(entitytype, entityid);
                    return Ok(response);
                }
                catch (PerfectAuditException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetSummaryByBookID
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpGet("{bookid}/summary")]
#if DOTNET2
        [ProducesResponseType(typeof(Analytics), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetSummaryByBookID(string bookId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfectAuditService.GetSummaryDataByBookId(bookId);
                    return Ok(response);
                }
                catch (PerfectAuditException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetTransactionByBookID
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpGet("{bookid}/transactions")]
#if DOTNET2
        [ProducesResponseType(typeof(Transaction), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetTransactionByBookID(string bookId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfectAuditService.GetTransactionByBookId(bookId);
                    return Ok(response);
                }
                catch (PerfectAuditException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// UploadStatement
        /// </summary>
        /// <param name="entitytype"></param>
        /// <param name="entityid"></param>
        /// <param name="bookPk"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/uploadstatement/{bookPk}/multiple")]
#if DOTNET2
        [ProducesResponseType(typeof(CommonResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> UploadStatement(string entitytype, string entityid, string bookPk, [FromBody]UploadStatementDetails files)
        {
            return await ExecuteAsync(async () =>
            {
            try
            {
                if (files == null || files.Filedetails != null && files.Filedetails.Count <= 0)
                    throw new ArgumentException("Must upload at least one file.", nameof(files));

                Logger.Info("executing UploadStatement in perfect audit syndication controller");
                  var response = await
                        PerfectAuditService.UploadStatement(entitytype, entityid,
                            files, /*filename,*/ bookPk);
                    return Ok(response);
                }
                catch (PerfectAuditException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// UploadStatement
        /// </summary>
        /// <param name="entitytype"></param>
        /// <param name="entityid"></param>
        /// <param name="bookPk"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/uploadstatement/{bookPk}/single")]
#if DOTNET2
        [ProducesResponseType(typeof(CommonResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> UploadStatement(string entitytype, string entityid, string bookPk, IFormFile file)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (file == null || file.Length <= 0)
                        throw new ArgumentException("Value cannot be null or whitespace.", nameof(file));
                    var filename = ContentDispositionHeaderValue
                        .Parse(file.ContentDisposition)
                        .FileName
                        .Trim('"');


                    Logger.Info("executing signle UploadStatement in perfect audit syndication controller");
                    var response = await
                          PerfectAuditService.UploadStatement(entitytype, entityid,
                              file.OpenReadStream().ReadAsBytes(), filename, bookPk);
                    return Ok(response);
                }
                catch (PerfectAuditException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
