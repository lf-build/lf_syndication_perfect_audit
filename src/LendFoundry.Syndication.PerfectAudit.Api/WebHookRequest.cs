﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PerfectAudit.Abstractions
{
    /// <summary>
    /// WebHookRequest
    /// </summary>
    public class WebHookRequest
    {
        /// <summary>
        /// Status
        /// </summary>
        public string Status = "VERIFICATION_COMPLETE";
        /// <summary>
        /// book_pk
        /// </summary>
        public int book_pk = 11917;
        /// <summary>
        /// uploaded_doc_pk
        /// </summary>
        public int uploaded_doc_pk = 57733;
    }
}
