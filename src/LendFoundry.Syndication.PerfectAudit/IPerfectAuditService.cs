﻿using System.Threading.Tasks;
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PerfectAudit
{
    public interface IPerfectAuditService
    {
        Task<CreateBook> CreateBook(string entityType, string entityId);
        Task<Transaction> GetTransactionData(string entityType, string entityId);
        Task<Analytics> GetSummaryDataByBookId(string bookId);
        Task<Transaction> GetTransactionByBookId(string bookId);
        Task UploadStatement(string entityType, string entityId, byte[] statement,string name, bool reCreateBook);
        Task<CommonResponse> UploadStatement(string entityType, string entityId, UploadStatementDetails statement, string bookPk);
        Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk);
 
    }
}