﻿
using LendFoundry.EventHub;
using LendFoundry.Foundation.Lookup;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System.Linq;
using System;
using LendFoundry.Syndication.PerfectAudit.Abstractions;
using LendFoundry.Syndication.PerfectAudit.Abstractions.Events;
using System.Net;

namespace LendFoundry.Syndication.PerfectAudit
{
    public class PerfectAuditService : IPerfectAuditService
    {
        public PerfectAuditService(IPerfectAuditSyndicationService perfectAuditSyndicationService, IPerfectAuditRepository perfectAuditRepository, IEventHubClient eventHub, ILookupService lookup, PerfectAuditConfiguration configuration)
        {
            PerfectAuditSyndicationService = perfectAuditSyndicationService;
            PerfectAuditRepository = perfectAuditRepository;
            EventHubClient = eventHub;
            LookupService = lookup;
            Configuration = configuration;
        }
        private IPerfectAuditSyndicationService PerfectAuditSyndicationService { get; }
        private IPerfectAuditRepository PerfectAuditRepository { get; }
        private IEventHubClient EventHubClient { get; }
        private ILookupService LookupService { get; }
        private PerfectAuditConfiguration Configuration { get; }
        public static string ServiceName { get; } = "perfectAudit";
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = LookupService.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
        public async Task<CreateBook> CreateBook(string entityType, string entityId)
        {
            CreateBook createbookresponse;
            entityType = EnsureEntityType(entityType);
            try
            {
                var referencenumber = Guid.NewGuid().ToString("N");
                var bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);
                if (bookinfo != null)
                {
                    var deletebookresponse = await PerfectAuditSyndicationService.DeleteBook(entityType, entityId, bookinfo.BookPk);
                    PerfectAuditRepository.Remove(bookinfo);
                }
                createbookresponse = await PerfectAuditSyndicationService.CreateBook(entityType, entityId);
                if (createbookresponse.status == (int)HttpStatusCode.OK)
                {
                    PerfectAuditData newBook = new PerfectAuditData();
                    newBook.EntityId = entityId;
                    newBook.EntityType = entityType;
                    newBook.BookName = createbookresponse.cbresponse.name;
                    newBook.BookPk = createbookresponse.cbresponse.pk.ToString();
                    newBook.VerificationStatus = "BookCreated";
                    newBook.CreatedDate = DateTime.Now;
                    PerfectAuditRepository.Add(newBook);
                    await EventHubClient.Publish(new PerfectAuditBookCreated()
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = createbookresponse,
                        Request = entityId,
                        ReferenceNumber = referencenumber,
                        Name = ServiceName
                    });
                }
                else
                {
                    throw new PerfectAuditException(createbookresponse.message);
                }
                return createbookresponse;
            }
            catch (Exception ex)
            {

                throw new PerfectAuditException(ex.Message);
            }

        }

        public async Task UploadStatement(string entityType, string entityId, byte[] statement, string name, bool reCreateBook)
        {
            if (Configuration.UseSimulation)
            {
                await CreateBook(entityType, entityId);
                var bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);
                await EventHubClient.Publish(new PerfectAuditStatementUploaded()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = "",
                    Request = entityId,
                    ReferenceNumber = Guid.NewGuid().ToString("N"),
                    Name = ServiceName
                });
                var response = await PerfectAuditSyndicationService.GetAnalyticalData(bookinfo.EntityType, bookinfo.EntityId, bookinfo.BookPk);
                if (response.status == (int)HttpStatusCode.OK)
                {
                    await EventHubClient.Publish(new PerfectAuditTransactionPulled()
                    {
                        EntityId = bookinfo.EntityId,
                        EntityType = bookinfo.EntityType,
                        Response = response,
                        Request = bookinfo.BookPk,
                        ReferenceNumber = Guid.NewGuid().ToString("N"),
                        Name = ServiceName
                    });
                }
            }
            else
            {
                entityType = EnsureEntityType(entityType);
                if (statement.Length == 0)
                    throw new ArgumentNullException("File length can not be zero");
                try
                {
                    var referencenumber = Guid.NewGuid().ToString("N");
                    var bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);

                    if (bookinfo != null && reCreateBook)
                    {
                        await PerfectAuditSyndicationService.DeleteBook(entityType, entityId, bookinfo.BookPk);
                        await PerfectAuditSyndicationService.CreateBook(entityType, entityId);
                        bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);
                    }

                    if (bookinfo == null)
                    {
                        await CreateBook(entityType, entityId);
                        bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);
                    }

                    var uploadresponse = await PerfectAuditSyndicationService.UploadStatement(entityType, entityId, bookinfo.BookPk, statement, name);

                    if (uploadresponse?.response?.status != (int)HttpStatusCode.BadRequest)
                    {
                        await EventHubClient.Publish(new PerfectAuditStatementUploaded()
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = uploadresponse,
                            Request = entityId,
                            ReferenceNumber = referencenumber,
                            Name = ServiceName
                        });

                    }
                    else
                    {
                        throw new PerfectAuditException(uploadresponse?.response?.message);
                    }
                }
                catch (Exception ex)
                {
                    throw new PerfectAuditException(ex.Message);

                }
            }
        }
        public async Task<Transaction> GetTransactionData(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);
            try
            {
                var referencenumber = Guid.NewGuid().ToString("N");
                var bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);
                if (bookinfo == null)
                    throw new PerfectAuditException("Book is not created for :" + entityId);
                var response = await PerfectAuditSyndicationService.GetTransaction(entityType, entityId, bookinfo.BookPk);
                if (response.status == (int)HttpStatusCode.OK)
                {
                    await EventHubClient.Publish(new PerfectAuditTransactionPulledTransaction()
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = response,
                        Request = entityId,
                        ReferenceNumber = referencenumber,
                        Name = ServiceName
                    });
                    return response;
                }
                else
                {
                    throw new PerfectAuditException(response.message);
                }
            }
            catch (Exception ex)
            {
                throw new PerfectAuditException(ex.Message);
            }
        }
        public async Task<Analytics> GetSummaryDataByBookId(string bookId)
        {
            if (bookId == null)
            {
                throw new PerfectAuditException("BookId is null :" + bookId);
            }
            try
            {
                var referencenumber = Guid.NewGuid().ToString("N");
                var bookinfo = await PerfectAuditRepository.GetByBookId(bookId);
                if (bookinfo == null)
                {
                    throw new PerfectAuditException("BookInfo not found for bookid :" + bookId);
                }
                bookinfo.EntityType = EnsureEntityType(bookinfo.EntityType);
                var response = await PerfectAuditSyndicationService.GetAnalyticalData(bookinfo.EntityType, bookinfo.EntityId, bookinfo.BookPk);
                if (response.status == (int)HttpStatusCode.OK)
                {
                    await EventHubClient.Publish(new PerfectAuditTransactionPulled()
                    {
                        EntityId = bookinfo.EntityId,
                        EntityType = bookinfo.EntityType,
                        Response = response,
                        Request = bookId,
                        ReferenceNumber = referencenumber,
                        Name = ServiceName
                    });
                    return response;
                }
                else
                {
                    throw new PerfectAuditException(response.message);
                }
            }
            catch (Exception ex)
            {
                throw new PerfectAuditException(ex.Message);
            }
        }
        public async Task<Transaction> GetTransactionByBookId(string bookId)
        {
            if (bookId == null)
            {
                throw new PerfectAuditException("BookId is null :" + bookId);
            }
            try
            {
                var referencenumber = Guid.NewGuid().ToString("N");
                var bookinfo = await PerfectAuditRepository.GetByBookId(bookId);
                if (bookinfo == null)
                {
                    throw new PerfectAuditException("BookInfo not found for bookid :" + bookId);
                }
                bookinfo.EntityType = EnsureEntityType(bookinfo.EntityType);
                var response = await PerfectAuditSyndicationService.GetTransaction(bookinfo.EntityType, bookinfo.EntityId, bookinfo.BookPk);
                if (response.status == (int)HttpStatusCode.OK)
                {
                    await EventHubClient.Publish(new PerfectAuditTransactionPulledTransaction()
                    {
                        EntityId = bookinfo.EntityId,
                        EntityType = bookinfo.EntityType,
                        Response = response,
                        Request = bookId,
                        ReferenceNumber = referencenumber,
                        Name = ServiceName
                    });
                    return response;
                }
                else
                {
                    throw new PerfectAuditException(response.message);
                }
            }
            catch (Exception ex)
            {
                throw new PerfectAuditException(ex.Message);
            }
        }
        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, UploadStatementDetails statement, /*string name,*/ string bookPk)
        {
            if (string.IsNullOrEmpty(bookPk))
            {
                throw new PerfectAuditException("BookId is null or empty :" + bookPk);
            }

            var bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);

            if (bookinfo == null)
            {
                throw new PerfectAuditException("bookinfo is not found for bookId :" + bookPk);
            }
            CommonResponse uploadresponse;

            if (Configuration.UseSimulation)
            {
                uploadresponse = new CommonResponse
                {
                    code = 200,
                    message = "OK"
                };

                var request = new WebHookRequest
                {
                    Status = "VERIFICATION_COMPLETE",
                    book_pk = Convert.ToInt32(bookPk),
                    uploaded_doc_pk = 57733
                };

               var response = await PerfectAuditSyndicationService.CallWebHook(request);

            }
            else
            {
                entityType = EnsureEntityType(entityType);
                if (statement.Filedetails.Count == 0)
                    throw new ArgumentNullException("File length can not be zero");
                try
                {
                    var referencenumber = Guid.NewGuid().ToString("N");

                    uploadresponse = await PerfectAuditSyndicationService.UploadStatement(entityType, entityId, bookinfo.BookPk, statement.Filedetails/*, name*/);

                    if (uploadresponse?.response?.status != (int)HttpStatusCode.BadRequest)
                    {
                        await EventHubClient.Publish(new PerfectAuditStatementUploaded()
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = uploadresponse,
                            Request = entityId,
                            ReferenceNumber = referencenumber,
                            Name = ServiceName
                        });

                    }
                    else
                    {
                        throw new PerfectAuditException(uploadresponse?.response?.message);
                    }
                }
                catch (Exception ex)
                {
                    throw new PerfectAuditException(ex.Message);

                }
            }
            return uploadresponse;
        }

        public async Task<CommonResponse> UploadStatement(string entityType, string entityId, byte[] statement, string name, string bookPk)
        {
            if (string.IsNullOrEmpty(bookPk))
            {
                throw new PerfectAuditException("BookId is null or empty :" + bookPk);
            }

            var bookinfo = await PerfectAuditRepository.GetByEntityId(entityType, entityId);

            if (bookinfo == null)
            {
                throw new PerfectAuditException("bookinfo is not found for bookId :" + bookPk);
            }
            CommonResponse uploadresponse;

            if (Configuration.UseSimulation)
            {
                uploadresponse = new CommonResponse
                {
                    code = 200,
                    message = "OK"
                };

                var request = new WebHookRequest
                {
                    Status = "VERIFICATION_COMPLETE",
                    book_pk = Convert.ToInt32(bookPk),
                    uploaded_doc_pk = 57733
                };

                var response = await PerfectAuditSyndicationService.CallWebHook(request);

            }
            else
            {
                entityType = EnsureEntityType(entityType);
                if (statement.Length == 0)
                    throw new ArgumentNullException("File length can not be zero");
                try
                {
                    var referencenumber = Guid.NewGuid().ToString("N");

                    uploadresponse = await PerfectAuditSyndicationService.UploadStatement(entityType, entityId, bookinfo.BookPk, statement, name);

                    if (uploadresponse?.response?.status != (int)HttpStatusCode.BadRequest)
                    {
                        await EventHubClient.Publish(new PerfectAuditStatementUploaded()
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = uploadresponse,
                            Request = entityId,
                            ReferenceNumber = referencenumber,
                            Name = ServiceName
                        });

                    }
                    else
                    {
                        throw new PerfectAuditException(uploadresponse?.response?.message);
                    }
                }
                catch (Exception ex)
                {
                    throw new PerfectAuditException(ex.Message);

                }
            }
            return uploadresponse;
        }

    }
}
