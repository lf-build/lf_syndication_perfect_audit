﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;
using Newtonsoft.Json;
using LendFoundry.Syndication.PerfectAudit.Abstractions;
namespace LendFoundry.Syndication.PerfectAudit
{
    public class PerfectAuditListener
    {
        public PerfectAuditListener(
              ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IConfigurationServiceFactory configurationFactory
            )
        {
            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);

            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ConfigurationFactory = configurationFactory;
        }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);
                    var reader = new StaticTokenReader(token.Value);
                    var eventHub = EventHubFactory.Create(reader);
                    var configuration = ConfigurationFactory.Create<PerfectAuditConfiguration>(Settings.ServiceName, reader).Get();

                    if (configuration != null && configuration.events != null)
                    {
                        configuration
                       .events
                       .ToList().ForEach(events =>
                       {
                      //     eventHub.On(events.Name, ProcessEvent(events, logger, eventHub));
                           logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                       });

                        logger.Info("-------------------------------------------");
                    }
                    else
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    eventHub.StartAsync();
                });

                logger.Info("PerfectAudit listener started");
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process PerfectAudit listener", ex);
                logger.Info("\n PerfectAudit listener  is working yet and waiting new event\n");
                Start();
            }
        }
        //private Action<EventInfo> ProcessEvent(
        //  EventConfiguration eventConfiguration,
        //  ILogger logger,
        //  IEventHubClient eventHub)
        //{
        //    return async @event =>
        //    {
        //        try
        //        {
        //            string responseData = eventConfiguration.Response.FormatWith(@event); //posted data
        //            var entityId = eventConfiguration.EntityId.FormatWith(@event);
        //            var entityType = eventConfiguration.EntityType.FormatWith(@event);
        //            var Name = eventConfiguration.Name.FormatWith(@event); //DocuSign event name

        //            object data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);

        //            if (!string.IsNullOrEmpty(eventConfiguration.CompletionEventName))
        //            {
        //                var objDocusignEventRequest = ExecuteRequest<DocusignEventRequest>(data.ToString());
        //                if (objDocusignEventRequest.Status == TemplateConfiguration.DocusignEventsType.Completed.ToString())
        //                {
        //                    byte[] fileByte = Convert.FromBase64String(objDocusignEventRequest.DocumentBytes);
        //                    string fileName = String.Format(eventConfiguration.DocumentNameFormat, entityId);

        //                    await eventHub.Publish(eventConfiguration.CompletionEventName, new
        //                    {
        //                        EntityId = entityId,
        //                        EntityType = entityType,
        //                        Response = new { DocumentContent = fileByte, DocumentName = fileName },
        //                        Request = objDocusignEventRequest,
        //                        ReferenceNumber = Guid.NewGuid().ToString("N")
        //                    });
        //                }
        //            }

        //            logger.Info($"Entity Id: #{entityId}");
        //            logger.Info($"Response Data : #{data}");
        //            logger.Info($"DocuSign event {Name} completed for {entityId} ");

        //        }
        //        catch (Exception ex)
        //        {
        //            logger.Error($"Unhadled exception while listening event {@event.Name}", ex);
        //        }
        //    };
        //}
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
    }


}
